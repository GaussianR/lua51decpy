class Conditional(object):
 """Conditional"""

 """Class constructor"""
 def __init__(self, cond, fpc, tpc):
  self.__begin_pc = fpc
  self.__end_pc = tpc
  self.__cond = cond


 def GetBegin(self):
  return self.__begin_pc


 def GetEnd(self):
  return self.__end_pc


 def GetPC(self):
  return self.__begin_pc - 2


 def Invert(self):
  return Conditional(self.__cond.Invert(), self.__begin_pc, self.__end_pc)


 def Eval(self, f):
  return self.__cond.Eval(f)


class OrConditional(Conditional):
 """Or conditional"""

 """Class constructor"""
 def __init__(self, a, b):
  Conditional.__init__(self, None, b.GetBegin(), b.GetEnd())
  self.__a = a
  self.__b = b


 def Invert(self):
  return AndConditional(self.__a.Invert(), self.__b.Invert())

 def Eval(self, f):
  return '(%s or %s)' % (self.__a.Eval(f), self.__b.Eval(f))


class AndConditional(Conditional):
 """And conditional"""

 """Class constructor"""
 def __init__(self, a, b):
  Conditional.__init__(self, None, b.GetBegin(), b.GetEnd())
  self.__a = a
  self.__b = b


 def Invert(self):
  return OrConditional(self.__a.Invert(), self.__b.Invert())


 def Eval(self, f):
  return '(%s and %s)' % (self.__a.Eval(f), self.__b.Eval(f))