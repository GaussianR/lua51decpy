#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import lfunc
import lcond
import lobject
import lundump
import lopcodes
import lregister
import linstruction
from luaconf import *

ARG_FMT = '_ARG_%u_'
LVAR_FMT = '_LVAR_%u_'
CLOSURE_FMT = '_CLOSURE_%u_'

class DecompileError(Exception):
 """Decompile exception"""

 def __init__(self, msg):
  self.message = msg

class Decompile(object):
 """Lua decompile script class"""

 """Constructor class"""
 def __init__(self, filename):
  p = lundump.Undump(filename).GetMainProto()
  p.SetVariableArg(VARARG_HASARG)

  self.__string_constants = set()
  self.__load_string_constants(p)
  self.__closure_count = 0
  self.__locvar_count = 0
  self.__code = {}

  self.__main = lfunc.Closure(p, 0)
  self.__set_closure_params(self.__main)

  self.__parse_closure(self.__main)


 """Get next local variable name"""
 def GetNextLocVarName(self):

  name = LVAR_FMT % self.__locvar_count
  self.__locvar_count+=1

  while name in self.__string_constants:
   name = LVAR_FMT % self.__locvar_count
   self.__locvar_count+=1

  return name


 """Get next closure name"""
 def GetNextClosureName(self):

  name = CLOSURE_FMT % self.__closure_count
  self.__closure_count+=1

  while name in self.__string_constants:
   name = CLOSURE_FMT % self.__closure_count
   self.__closure_count+=1

  return name

 def __append_line_debug(self, f, content, line):
  print('%u += %s%s' % (line, ' ' * f.GetIndentation(), content))


 def __append_line_release(self, f, content, line):

  content = '%s%s ' % (' ' * f.GetIndentation(), content)

  if line not in self.__code:
   self.__code[line] = content
  else:
   self.__code[line]+= content


 def AppendToLine(self, f, content, line = None):
  line = line if line != None else f.GetLine()

  if __debug__:
   self.__append_line_debug(f, content, line)
  else:
   self.__append_line_release(f, content, line)


 def GenerateSource(self):
  return '\n'.join([self.__code[i].rstrip() if i in self.__code else '' for i in range(0, max(sorted(self.__code))+1)])


 def __set_closure_params(self, f):
  p = f.GetPrototype()
  count = 0

  for i in range(0, p.GetParamCount()):

   name = ARG_FMT % count
   count+=1

   while name in self.__string_constants:
    name = ARG_FMT % count
    count+=1

   f.SetParamName(i, name)


 """Load string constants"""
 def __load_string_constants(self, p):

  constants = []
  for k in p.GetConstants():
   if k.IsString():
    constants.append(k.GetValue())

  self.__string_constants.update(constants)

  for np in p.GetPrototypes():
   self.__load_string_constants(np)


 """Process closure instructions"""
 def __parse_closure(self, f):

  if __debug__:
   p = f.GetPrototype()
   lineinfo = p.GetLineInfo()

   print ('Line info:')
   for line in sorted(lineinfo):
    print ('  %s: %s' % (lineinfo[line], line))

   print ('')

  while f.FetchLine():
   while f.SaveLinePC():
    u = f.FetchCodeUnit()
    self.__process_opcode(f, lopcodes.GET_OPCODE(u), u)


 """Get register or constant"""
 def __get_rk(self, f, x):
  p = f.GetPrototype()

  if lopcodes.ISK(x):
   k_id = lopcodes.INDEXK(x)
   k = p.GetConstant(k_id)

   if k == None:
    raise DecompileError('Couldn\'t get global K%u for closure "%s".' % (k_id, p.GetName()))
   else:
    return lregister.ConstantRegister(k)
  else:
   return f.GetRegister(x)


 """Get register or constant representation"""
 def __get_rk_repr(self, f, x):
  p = f.GetPrototype()

  if lopcodes.ISK(x):
   k_id = lopcodes.INDEXK(x)
   k = p.GetConstant(k_id)

   if k == None:
    raise DecompileError('Couldn\'t get global K%u for closure "%s".' % (k_id, p.GetName()))
   else:
    return 'K%u | %s' % (k_id, k)
  else:
   return 'R%u' % x



 """Get call params"""
 def __get_call(self, f, a, b):
  if b > 2:
   return (a, a + b - 2)
  elif b == 2:
   return a
  elif b == 1:
   return None
  elif b == 0:
   return (a, f.GetTop()) if a != f.GetTop() else a
  else:
   raise DecompileError('Invalid CALL instruction for closure "%s".' % (f.GetPrototype().GetName()))


 """Get call params representation"""
 def __get_call_repr(self, a, b):
  if b > 2:
   return 'R%u to R%u' % (a, a + b - 2)
  elif b == 2:
   return 'R%u' % a
  elif b == 1:
   return ''
  elif b == 0:
   return 'R%u to top' % a


 """Process boolean ternary"""
 def __process_bool_ternary(self, f):

  i = f.LookAhead()
  if i == None:
   return None

  a = lopcodes.GETARG_A(i) 
  if lopcodes.GET_OPCODE(i) != lopcodes.OP_LOADBOOL or not lopcodes.GETARG_C(i):
   return None

  i = f.LookAhead(1)
  if i == None:
   return None

  return (a, lopcodes.GETARG_B(i) != 0) if lopcodes.GET_OPCODE(i) == lopcodes.OP_LOADBOOL else None


 """Move opcode"""
 def __process_move(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  r = lregister.ReferencedRegister(f.GetRegister(b))

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | MOVE R%u, R%u' % (p.GetName(), savedpc, a, b))


 def __process_loadk(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  bc = lopcodes.GETARG_Bx(i)

  k = p.GetConstant(bc)
  if k == None:
   raise DecompileError('Couldn\'t get K%u for closure "%s".' % (bc, p.GetName()))

  r = lregister.ConstantRegister(k)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | LOADK R%u, K%u | %s' % (p.GetName(), savedpc, a, bc, k))


 def __process_loadbool(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  r = lregister.ConstantRegister(lobject.TValue(LUA_TBOOLEAN, b != 0))

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   cond = '; PC = %u' % (savedpc+2) if c else ''
   print ('[%s] - %u | LOADBOOL R%u, %s%s' % (p.GetName(), savedpc, a, 'true' if b != 0 else 'false', cond))

 def __process_loadnil(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)

  if b > a:
   r = (a, b)
  else:
   r = (a, a)

  rg = range(r[0], r[1] + 1)

  if f.IsLastLinePC():
   names = []
   for i in rg:
    name = self.GetNextLocVarName()
    names.append(name)

    f.SetRegister(i, lregister.LocVarRegister(name))

   self.AppendToLine(f, linstruction.LocVarNilInstruction(names).Eval(f))
  else:
   value = lobject.TValue(LUA_TNIL)

   for i in rg:
    f.SetRegister(i, lregister.ConstantRegister(value))

  if __debug__:
   print ('[%s] - %u | LOADNIL R%u to R%u, nil' % (p.GetName(), savedpc, r[0], r[1]))


 def __process_getupval(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)

  f.SetRegister(a, lregister.UpvalueRegister(f.GetUpValue(b)))

  if __debug__:
   print ('[%s] - %u | GETUPVAL R%u, U%u' % (p.GetName(), savedpc, a, b))


 def __process_getglobal(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  bc = lopcodes.GETARG_Bx(i)

  k = p.GetConstant(bc)
  if k == None or not k.IsString():
   raise DecompileError('Couldn\'t get a valid global K%u for closure "%s".' % (bc, p.GetName()))

  r = lregister.GlobalRegister(k.GetValue())

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)


  if __debug__:
   print ('[%s] - %u | GETGLOBAL R%u, K%u | %s' % (p.GetName(), savedpc, a, bc, k))


 def __process_gettable(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  key = self.__get_rk(f, c)
  r = lregister.TableEntryRegister(f.GetRegister(b), key)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | GETTABLE R%u, R%u[%s]' % (p.GetName(), savedpc, a, b, self.__get_rk_repr(f, c)))


 def __process_setglobal(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  bc = lopcodes.GETARG_Bx(i)

  k = p.GetConstant(bc)
  r_a = f.GetRegister(a)
  if k == None or not k.IsString():
   raise DecompileError('Couldn\'t get a valid global K%u for closure "%s".' % (bc, p.GetName()))

  _abstract = False

  i = f.LookAheadLine()
  if i != None and lopcodes.GET_OPCODE(i) == lopcodes.OP_CLOSURE and a == lopcodes.GETARG_A(i):
   _abstract = True
   f.SetNextName(k.GetValue())

  if not _abstract:
   i = f.LookAhead()
   if i != None and lopcodes.GET_OPCODE(i) == lopcodes.OP_RETURN:
    _abstract = True

  if not _abstract:
   self.AppendToLine(f, linstruction.SetGlobalInstruction(k, r_a).Eval(f))

  if __debug__:
   print ('[%s] - %u | SETGLOBAL K%u | %s, R%u' % (p.GetName(), savedpc, bc, k, a))


 def __process_setupval(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)

  r_a = f.GetRegister(a)
  up = f.GetUpValue(b)

  if isinstance(up, lregister.LocVarRegister):
   self.AppendToLine(f, linstruction.SetUpvalInstruction(up, r_a).Eval(f))
  else:
   f.SetUpValue(b, r_a)

  if __debug__:
   print ('[%s] - %u | SETUPVAL U%u, R%u' % (p.GetName(), savedpc, b, a))


 def __process_settable(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  table = f.GetRegister(a)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)

  t = table.Cast(lregister.TableRegister)
  if t != None:
   t.Set(key_b, key_c)

  if f.IsLastLinePC():
   if table.IsA(lregister.TableRegister):
    name = self.GetNextLocVarName()
    self.AppendToLine(f, linstruction.LocVarInstruction(name, table).Eval(f))
    f.SetRegister(a, lregister.LocVarRegister(name))
   else:
    self.AppendToLine(f, linstruction.SetTableInstruction(table, key_b, key_c).Eval(f))

  if __debug__:
   print ('[%s] - %u | SETTABLE R%u[%s], %s' % (p.GetName(), savedpc, a, self.__get_rk_repr(f, b), self.__get_rk_repr(f, c)))


 def __process_newtable(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  r = lregister.TableRegister(b)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | NEWTABLE R%u, {} (%u, %u)' % (p.GetName(), savedpc, a, b, c))

 def __process_self(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  reg_b = f.GetRegister(b)
  key_c = self.__get_rk(f, c)

  f.SetRegister(a+1, lregister.ReferencedRegister(reg_b))
  f.SetRegister(a, lregister.SelfEntryRegister(reg_b, key_c))

  if __debug__:
   print ('[%s] - %u | SELF R%u, R%u; R%u, R%u[%s]' % (p.GetName(), savedpc, a+1, b, a, b, self.__get_rk_repr(f, c)))

 def __process_add(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)
  r = lregister.AddRegister(key_b, key_c)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | ADD R%u, %s + %s' % (p.GetName(), savedpc, a, self.__get_rk_repr(f, b), self.__get_rk_repr(f, c)))


 def __process_sub(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)
  r = lregister.SubRegister(key_b, key_c)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | SUB R%u, %s - %s' % (p.GetName(), savedpc, a, self.__get_rk_repr(f, b), self.__get_rk_repr(f, c)))


 def __process_mul(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)
  r = lregister.MulRegister(key_b, key_c)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | MUL R%u, %s * %s' % (p.GetName(), savedpc, a, self.__get_rk_repr(f, b), self.__get_rk_repr(f, c)))


 def __process_div(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)
  r = lregister.DivRegister(key_b, key_c)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | DIV R%u, %s / %s' % (p.GetName(), savedpc, a, self.__get_rk_repr(f, b), self.__get_rk_repr(f, c)))


 def __process_mod(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)
  r = lregister.ModRegister(key_b, key_c)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | MOD R%u, %s %% %s' % (p.GetName(), savedpc, a, self.__get_rk_repr(f, b), self.__get_rk_repr(f, c)))


 def __process_pow(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)
  r = lregister.PowRegister(key_b, key_c)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | POW R%u, %s ^ %s' % (p.GetName(), savedpc, a, self.__get_rk_repr(f, b), self.__get_rk_repr(f, c)))


 def __process_unm(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  r = lregister.NegRegister(f.GetRegister(b))

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | UNM R%u, -R%u' % (p.GetName(), savedpc, a, b))


 def __process_not(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  r = lregister.NotRegister(f.GetRegister(b))

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | NOT R%u, ~R%u' % (p.GetName(), savedpc, a, b))


 def __process_len(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  r = lregister.LengthRegister(f.GetRegister(b))

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | LEN R%u, #R%u' % (p.GetName(), savedpc, a, b))


 def __process_concat(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  r = lregister.ConcatRegister([f.GetRegister(j) for j in range(b, c+1)])


  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | CONCAT R%u, R%u-R%u' % (p.GetName(), savedpc, a, b, c))

 def __process_jmp(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  dest = f.GetPC() + lopcodes.GETARG_sBx(i)

  i = f.GetCodeUnit(dest)
  o = lopcodes.GET_OPCODE(i)
  if o == lopcodes.OP_TFORLOOP:
   if __debug__:
    print ('TLoop detected')

  if __debug__:
   print ('[%s] - %u | JMP %u' % (p.GetName(), savedpc, dest))


 def __process_cond_jmp(self, f, i, op, invop):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  o = lopcodes.GET_OPCODE(i)
  key_b = self.__get_rk(f, b)
  key_c = self.__get_rk(f, c)
  oname = 'EQ' if o == lopcodes.OP_EQ else (
      'LT' if o == lopcodes.OP_LT else 'LE')

  if not f.CheckPC():
   raise DecompileError('PC out of bounds while reading %s for closure "%s".' % (oname, p.GetName()))

  displ = lopcodes.GETARG_sBx(f.FetchCodeUnit())
  dest = f.GetPC() + displ

  cond = self.__process_bool_ternary(f) if displ == 1 else None

  #Process boolean ternary operator
  if cond != None:
   reg = lregister.BinaryConditionalRegister(key_b, key_c, a == 0 if cond[1] else a != 0, op, invop)
   f.SetRegister(cond[0], reg)
   f.SkipCodeUnit(2)
  else:
   reg = lregister.BinaryConditionalRegister(key_b, key_c, not a, op, invop)
   f.AddConditional(lcond.Conditional(reg, f.GetPC(), dest))

   if __debug__:
    operator = op if a else invop
    print ('[%s] - %u | %s if (%s %s %s) PC=%u [%d]' % (p.GetName(), savedpc, oname, self.__get_rk_repr(f, b), operator, self.__get_rk_repr(f, c), dest, displ))

   self.__process_cond_chain(f)


 def __process_eq(self, f, i):
  self.__process_cond_jmp(f, i, '==', '~=')


 def __process_lt(self, f, i):
  self.__process_cond_jmp(f, i, '<', '>=')


 def __process_le(self, f, i):
  self.__process_cond_jmp(f, i, '<=', '>')


 def __process_test(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  c = lopcodes.GETARG_C(i)

  if not f.CheckPC():
   raise DecompileError('PC out of bounds while reading TEST for closure "%s".' % p.GetName())

  displ = lopcodes.GETARG_sBx(f.FetchCodeUnit())
  dest = f.GetPC() + displ
  r_a = f.GetRegister(a)

  f.IsLastLinePC()
  reg = lregister.TestRegister(r_a, not c)
  f.AddConditional(lcond.Conditional(reg, f.GetPC(), dest))

  if __debug__:
   operator = '' if c != 0 else 'not '
   print ('[%s] - %u | TEST if %sR%u PC=%u [%d]' % (p.GetName(), savedpc, operator, a, dest, displ))

  self.__process_cond_chain(f)


 def __check_cond_jmp(self, f):
  last = f.GetLastLinePC()
  end = f.GetConditionals().peek().GetEnd()

  i = f.GetPC()
  while i < last:

   o = lopcodes.GET_OPCODE(f.GetCodeUnit(i))

   if lopcodes.ISCONDJMP(o):
    return True
   elif o == lopcodes.OP_JMP:
    return False
   else:
    i+=1

  return False


 def __pop_condition(self, f, stack):
  current = stack.pop()
  begin = current.GetBegin()

  while not stack.empty():
   next = stack.peek()
   if next.GetEnd() == begin:
    current = lcond.OrConditional(self.__pop_condition(f, stack).Invert(), current)
   elif next.GetEnd() == current.GetEnd():
    current = lcond.AndConditional(self.__pop_condition(f, stack), current)
   else:
    break

  return current


 def __process_cond_chain(self, f):
  if self.__check_cond_jmp(f):
   return False

  cond = self.__pop_condition(f, f.GetConditionals())

  if __debug__:
    print(cond.GetBegin(), cond.GetEnd(), cond.GetPC())

  self.AppendToLine(f, linstruction.IfInstruction(cond).Eval(f))


 def __process_testset(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)

  if not f.CheckPC():
   raise DecompileError('PC out of bounds while reading TESTSET for closure "%s".' % p.GetName())

  displ = lopcodes.GETARG_sBx(f.FetchCodeUnit())-1

  if __debug__:
   operator = '' if c else 'not '
   print ('[%s] - %u | TESTSET PC+= (%sR%u)(%d); if %sR%u then R%u, R%u' % (
   p.GetName(), savedpc, operator, b, displ, operator, b, a, b))


 def __process_call(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  op = lopcodes.GET_OPCODE(i)
  r = self.__get_call(f, a, c)
  s = self.__get_call(f, a + 1, b)


  r_a = f.GetRegister(a)
  r_s = f.GetRegisters(s)

  if r == None:
   self.AppendToLine(f, lregister.CallRegister(r_a, r_s).Eval(f))
  elif isinstance(r, tuple):
   call_r = lregister.CallRegister(r_a, r_s)

   if f.IsLastLinePC():
    names = []
    for i in range(r[0], r[1] + 1):
     name = self.GetNextLocVarName()
     names.append(name)

     f.SetRegister(i, lregister.LocVarRegister(name))

    self.AppendToLine(f, linstruction.LocVarMultiCallInstruction(names, call_r).Eval(f))
   else:
    f.SetRegister(r[0], lregister.MultiRegister(call_r, r[1] - r[0] + 1))

  else:
   call_r = lregister.CallRegister(r_a, r_s)

   if f.IsLastLinePC():
    name = self.GetNextLocVarName()
    orgline = line = linstruction.LocVarInstruction(name, call_r).Eval(f)
    line = re.sub(r'local\s+.+?\s+\=\s+require\(\"(.+?)\"\)', r'require "\1"', line)

    self.AppendToLine(f, line)
    f.SetRegister(r, lregister.LocVarRegister(name, call_r if orgline != line else None))
   else:
    f.SetRegister(r, call_r)

  if __debug__:
   r = self.__get_call_repr(a, c)
   if r:
    r = ' ' + r + ','

   print('[%s] - %u | %s%s R%u(%s)' % (p.GetName(), savedpc,
   'CALL' if op == lopcodes.OP_CALL else 'TAILCALL',
   r, a, self.__get_call_repr(a + 1, b)))


 def __process_return(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  r = self.__get_call(f, a, b)

  line = linstruction.ReturnInstruction(r).Eval(f)
  if line != None:
   self.AppendToLine(f, line)

  if __debug__:
   print (('[%s] - %u | RETURN %s' % (p.GetName(), savedpc, self.__get_call_repr(a, b))))


 def __process_forloop(self, f, i):
  pass


 def __process_forprep(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  dest = f.GetPC() + lopcodes.GETARG_sBx(i)

  init = f.GetRegister(a)
  step = f.GetRegister(a+2)
  limit = f.GetRegister(a+1)

  name = self.GetNextLocVarName()
  lvar = lregister.LocVarRegister(name)

  f.SetRegister(a, lvar)
  f.SetRegister(a+3, lregister.ReferencedRegister(lvar))
  self.AppendToLine(f, linstruction.NumericForInstruction(lvar, init, limit, step).Eval(f))

  if __debug__:
   print (('[%s] - %u | FORPREP R%u = ??; R%u < R%u; R%u+=R%u; PC=%u' % (p.GetName(), savedpc, a, a, a+1, a, a+2, dest)))


 def __process_tforloop(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()

  if __debug__:
   print (('[%s] - %u | TFORLOOP End' % (p.GetName(), savedpc)))


 def __process_setlist(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  c = lopcodes.GETARG_C(i)
  table = f.GetRegister(a)
  offset = (c - 1)*LFIELDS_PER_FLUSH

  if b == 0:
   r = (offset, offset + f.GetTop() - a - 1)
   q = (a + 1, f.GetTop())
  elif b == 1:
   r = (offset, offset)
   q = (a + 1, a + 1)
  elif b > 1:
   r = (offset, offset + b - 1)
   q = (a + 1, a + b)
  else:
   raise DecompileError('Invalid B param while processing SETLIST for closure "%s".' % p.GetName())

  t = table.Cast(lregister.TableRegister)
  if t != None:
   j = r[0]
   k = q[0]
   while j <= r[1]:
    t.Insert(j, f.GetRegister(k))
    j+=1
    k+=1

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, table).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))

  if __debug__:
   print (('[%s] - %u | SETLIST R%u[%u to %u], R%u to R%u' % (p.GetName(), savedpc, a, r[0], r[1], q[0], q[1])))



 def __process_close(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)

  f.Close(a)

  if __debug__:
   print ('[%s] - %u | CLOSE R%u to R%u' % (p.GetName(), savedpc, a, f.GetTop()))


 def __process_closure(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  bc = lopcodes.GETARG_Bx(i)

  np = p.GetPrototype(bc)
  cl = lfunc.Closure(np, f.GetIndentation() + 2)

  self.__set_closure_params(cl)

  if np == None:
   raise DecompileError('Couldn\'t get prototype %u for closure "%s".' % (bc, p.GetName()))

  n = np.GetUpValueCount()
  upvalues = [None] * n

  for j in range(0, n):

   if not f.CheckPC():
    raise DecompileError('PC out of bounds while reading %u upvalue for closure "%s" from closure "%s".' % (j, p.GetName(), np.GetName()))

   i = f.FetchCodeUnit()
   b = lopcodes.GETARG_B(i)
   o = lopcodes.GET_OPCODE(i)

   upvalue = ('U%u' % b) if o == lopcodes.OP_GETUPVAL else (
             ('R%u' % b) if o == lopcodes.OP_MOVE else None)

   if upvalue == None:
    raise DecompileError('Unknown upvalue found <%u> at %u+%03X for closure "%s".' % (i, f.GetSavedPC(), j, p.GetName()))

   upvalues[j] = upvalue
   cl.SetUpValue(j, f.GetUpValue(b) if o == lopcodes.OP_GETUPVAL else f.GetRegister(b))

  r_c = lregister.ClosureRegister(cl)
  fline = np.GetFirstLine()
  name = f.GetNextName()

  if name != None:
   self.AppendToLine(f, linstruction.SetGlobalInstruction(lobject.TValue(LUA_TSTRING, name), r_c).Eval(f), fline)
  else:
   name = self.GetNextClosureName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r_c).Eval(f), fline)
   f.SetRegister(a, lregister.LocVarRegister(name))

  f.SetNextName(None)
  self.__parse_closure(cl)
  self.AppendToLine(f, 'end -- End of %s' % name, np.GetLastLine())

  if __debug__:
   print ('[%s] - %u | CLOSURE R%u, %s (%s) <%s>' % (p.GetName(), savedpc, a, name, np.GetName(), ', '.join(upvalues)))


 def __process_vararg(self, f, i):
  p = f.GetPrototype()
  savedpc = f.GetSavedPC()
  a = lopcodes.GETARG_A(i)
  b = lopcodes.GETARG_B(i)
  flags = p.IsVariableArg()

  if (flags & VARARG_HASARG) == 0:
   raise DecompileError('Tried to obtain vararg table within non-vararg closure "%s".' % p.GetName())

  r = lregister.VarargRegister(flags)

  if f.IsLastLinePC():
   name = self.GetNextLocVarName()
   self.AppendToLine(f, linstruction.LocVarInstruction(name, r).Eval(f))
   f.SetRegister(a, lregister.LocVarRegister(name))
  else:
   f.SetRegister(a, r)

  if __debug__:
   print ('[%s] - %u | VARARG %s, ...' % (p.GetName(), savedpc, self.__get_call_repr(a, b)))


 def __process_opcode(self, f, o, u):
  handlers = [
   self.__process_move,
   self.__process_loadk,
   self.__process_loadbool,
   self.__process_loadnil,
   self.__process_getupval,
   self.__process_getglobal,
   self.__process_gettable,
   self.__process_setglobal,
   self.__process_setupval,
   self.__process_settable,
   self.__process_newtable,
   self.__process_self,
   self.__process_add,
   self.__process_sub,
   self.__process_mul,
   self.__process_div,
   self.__process_mod,
   self.__process_pow,
   self.__process_unm,
   self.__process_not,
   self.__process_len,
   self.__process_concat,
   self.__process_jmp,
   self.__process_eq,
   self.__process_lt,
   self.__process_le,
   self.__process_test,
   self.__process_testset,
   self.__process_call,
   self.__process_call,
   self.__process_return,
   self.__process_forloop,
   self.__process_forprep,
   self.__process_tforloop,
   self.__process_setlist,
   self.__process_close,
   self.__process_closure,
   self.__process_vararg,
  ]

  if o > lopcodes.OP_VARARG:
   raise DecompileError('PC%u - Invalid opcode %u for closure "%s".' % (f.GetSavedPC(), o, f.GetPrototype().GetName()))

  handlers[o](f, u)
