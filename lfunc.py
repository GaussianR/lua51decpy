#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import lobject
import lregister
from luaconf import LUA_TNIL

class Proto(object):
 """Closure prototype"""

 def __init__(self, name, fline, lline, nups, nparams, isvararg, maxstacksize):
  self.__name = name
  self.__first_line = fline
  self.__last_line = lline
  self.__upvalue_count = nups
  self.__param_count = nparams
  self.__is_vararg = isvararg
  self.__max_stacksize = maxstacksize

 def SetCode(self, code):
  self.__code = code
  self.__codesize = len(code)

 def SetConstants(self, constants):
  self.__constants = constants
  self.__constant_count = len(constants)

 def SetPrototypes(self, prototypes):
  self.__prototypes = prototypes
  self.__prototype_count = len(prototypes)

 def SetLineInfo(self, lineinfo):
  self.__lineinfo = lineinfo

 def SetLocalVars(self, locvars):
  self.__local_vars = locvars
  self.__loval_var_count = len(locvars)

 def SetUpValueNames(self, upvalues):
  self.__upvalue_names = upvalues

 def GetCode(self):
  return self.__code

 def GetCodeSize(self):
  return self.__codesize

 def GetCodeUnit(self, pc):
  return self.__code[pc]

 def GetConstant(self, id):
  if id >= self.__constant_count:
   return None

  return self.__constants[id]

 def GetConstants(self):
  return self.__constants

 def GetPrototype(self, id):
  if id >= self.__prototype_count:
   return None

  return self.__prototypes[id]

 def GetPrototypes(self):
  return self.__prototypes

 def GetLineInfo(self):
  return self.__lineinfo

 def GetLineInfoCount(self):
  return self.__lineinfo_count

 def GetName(self):
  return self.__name

 def GetFirstLine(self):
  return self.__first_line

 def GetLastLine(self):
  return self.__last_line

 def GetUpValueCount(self):
  return self.__upvalue_count

 def GetParamCount(self):
  return self.__param_count

 def GetLocalVarCount(self):
  return self.__loval_var_count

 def GetMaxStackSize(self):
  return self.__max_stacksize

 def IsVariableArg(self):
  return self.__is_vararg

 def SetVariableArg(self, flag):
  self.__is_vararg = flag


class Closure(object):
 """Closure class"""

 """Class constructor"""
 def __init__(self, p, indent):
  stacksize = p.GetMaxStackSize()

  self.__upvalues = lregister.RegisterStack('UpValues', 'U', p.GetUpValueCount())
  self.__registers = lregister.RegisterStack('Registers', 'R', stacksize)
  self.__params = [None] * p.GetParamCount()
  self.__conditionals = lobject.Stack()
  self.__indentation = indent
  self.__next_name = None
  self.__prototype = p
  self.__line_iter = 0
  self.__savedpc = 0
  self.__code = ''
  self.__top = 0
  self.__pc = 0

  for i in range(0, stacksize):
   self.__registers.SetEntry(i, lregister.ConstantRegister(lobject.TValue(LUA_TNIL)))

  # Load line info
  self.__lines_info = p.GetLineInfo()

  self.__lines = sorted(self.__lines_info)
  self.__line_count = len(self.__lines)


 def GetPrototype(self):
  return self.__prototype


 def SetParamName(self, i, name):
  self.SetRegister(i, lregister.ArgumentRegister(name))
  self.__params[i] = name


 def GetParamName(self, i):
  return self.__params[i]


 def GetParamNames(self):
  return self.__params


 def CheckPC(self):
  return self.__pc < self.__prototype.GetCodeSize()


 def SaveLinePC(self):
  self.__savedpc = self.__pc
  return self.__savedpc <= self.__line_last_pc


 def GetPC(self):
  return self.__pc


 def IsLastLinePC(self):
  if __debug__:
   print ('PC = %u, SAVEDPC = %u, LASTPC = %u, LINE = %u' % (self.__pc, self.__savedpc, self.__line_last_pc, self.__line))

  return self.__savedpc == self.__line_last_pc


 def GetLastLinePC(self):
  return self.__line_last_pc


 def GetSavedPC(self):
  return self.__savedpc


 def GetCodeUnit(self, pc = None):
  return self.__prototype.GetCodeUnit(pc) if pc != None else self.__prototype.GetCodeUnit(self.__pc)


 def SkipCodeUnit(self, i = 1):
  self.__pc+=i


 def FetchCodeUnit(self):
  i = self.GetCodeUnit()
  self.SkipCodeUnit()
  return i


 def LookAhead(self, i = 0):
  if (self.__pc+i) >= self.__prototype.GetCodeSize():
   return None

  return self.__prototype.GetCodeUnit(self.__pc+i)


 def GetLine(self):
  return self.__line


 def CheckLine(self):
  return self.__line_iter < self.__line_count


 def FetchLine(self):
  if self.__line_iter >= self.__line_count:
   return False

  self.__line = self.__lines[self.__line_iter]
  self.__line_info = self.__lines_info[self.__line]

  self.__pc = self.__line_info[0]
  self.__line_last_pc = self.__line_info[1]

  self.__line_iter+=1
  return True


 def LookAheadLine(self, i = 0, j = 0):
  if self.__line_iter >= self.__line_count:
   return None

  pc = self.__lines_info[self.__lines[self.__line_iter+i]][0] + j
  if pc >= self.__prototype.GetCodeSize():
   return None

  return self.__prototype.GetCodeUnit(pc)


 def GetTop(self):
  return self.__top


 def Close(self, a):
  for i in range(a, self.__top + 1):
   self.__registers.SetEntry(i, lregister.ConstantRegister(i, -1, lobject.TValue(LUA_TNIL, None)))

  if a > 0:
   self.__top = a - 1


 def SetRegister(self, id, reg):
  self.__top = max(self.__top, id)
  self.__registers.SetEntry(id, reg)


 def GetRegister(self, id):
  return self.__registers.GetEntry(id)


 def GetRegisters(self, r):
  if r == None:
   return None
  elif isinstance(r, tuple):

   i = r[0]
   registers = []
   while i <= r[1]:
    reg = self.__registers.GetEntry(i).GetSelf()
    registers.append(reg)

    i+=(reg.GetSize() if isinstance(reg, lregister.MultiRegister) else 1)

   return registers
  else:
   return self.__registers.GetEntry(r)


 def SetUpValue(self, id, up):
  self.__upvalues.SetEntry(id, up)


 def GetUpValue(self, id):
  return self.__upvalues.GetEntry(id)


 def GetIndentation(self):
  return self.__indentation


 def AddConditional(self, conditional):
  self.__conditionals.push(conditional)


 def GetConditionals(self):
  return self.__conditionals


 def SetNextName(self, name):
  self.__next_name = name


 def GetNextName(self):
  return self.__next_name


 def GetCurrentLine(self):
  return self.__line


 def WriteLine(self, content, line = None):

  line = self.__line if line == None else line
  out = '%u += %s%s\n' % (line, ' ' * self.__indentation, content)
  #out = '%s%s\n' % (' ' * self.__indentation, line)

  #if __debug__:
  sys.stdout.write(out)
  #else:
   #self.__code+=out


#class Scope(object):
# """Scope class"""

# def __init__(self, indent, cl, lineinfo, pcrange):
#  self.__closure = cl
#  