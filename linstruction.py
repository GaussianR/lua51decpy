#!/usr/bin/python
# -*- coding: utf-8 -*-

from lregister import RegisterBase
from lregister import SelfEntryRegister

class InstructionError(Exception):
 """Instruction exception class"""

 def __init__(self, msg):
  self.message = msg


class InstructionBase(object):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self):
  pass


class SetTableInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, table, key, value):
  InstructionBase.__init__(self)
  self.__table = table
  self.__key = key
  self.__value = value


 def Eval(self, f):
  key = self.__key.Eval(f) if isinstance(self.__key, RegisterBase) else str(self.__key)
  key = '.%s' % (key.replace('"', '')) if key.find('"') != -1 else '[%s]' % key


  return '%s%s = %s' % (
   self.__table.Eval(f), key,
   self.__value.Eval(f) if isinstance(self.__value, RegisterBase)
   else str(self.__value)
  )


class SetGlobalInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, k, r):
  InstructionBase.__init__(self)
  self.__constant = k
  self.__register = r


 def Eval(self, f):
  return '%s = %s' % (self.__constant.GetValue(), self.__register.Eval(f))


class LocVarNilInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, names):
  InstructionBase.__init__(self)
  self.__names = names


 def Eval(self, f):
  return 'local %s' % (', '.join(self.__names))


class LocVarMultiCallInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, names, call):
  InstructionBase.__init__(self)
  self.__names = names
  self.__call = call


 def Eval(self, f):
  return 'local %s = %s' % (', '.join(self.__names), self.__call.Eval(f))


class LocVarInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, name, key):
  InstructionBase.__init__(self)
  self.__key = key
  self.__name = name


 def Eval(self, f):
  return 'local %s = %s' % (self.__name, self.__key.Eval(f))


class IfInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, reg):
  InstructionBase.__init__(self)
  self.__reg = reg


 def Eval(self, f):
  return 'if %s then' % self.__reg.Eval(f)


class NumericForInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, initvar, initval, limit, step):
  InstructionBase.__init__(self)
  self.__init_var = initvar
  self.__init_val = initval
  self.__limit = limit
  self.__step = step


 def Eval(self, f):
  return 'for %s = %s, %s, %s do' % (self.__init_var.Eval(f), self.__init_val.Eval(f), self.__limit.Eval(f), self.__step.Eval(f))


class SetUpvalInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, up, reg):
  InstructionBase.__init__(self)
  self.__upvalue = up
  self.__reg = reg


 def Eval(self, f):
  return '%s = %s' % (self.__upvalue.Eval(f), self.__reg.Eval(f))


class ReturnInstruction(InstructionBase):
 """Instruction base class"""

 """Class constructor"""
 def __init__(self, r):
  InstructionBase.__init__(self)
  self.__results = r


 def Eval(self, f):

  if self.__results == None:
   return None
  elif isinstance(self.__results, tuple):
   return 'return ' + ', '.join([r.Eval(f) for r in f.GetRegisters(self.__results)])
  else:
   return 'return ' + f.GetRegister(self.__results).Eval(f)
