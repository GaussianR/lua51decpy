#!/usr/bin/python
# -*- coding: utf-8 -*-

import lregister
from luaconf import *

class TValue(object):
 """TValue"""

 """Class constructor"""
 def __init__(self, tt, o = None):
  self.__tt = tt
  self.__o = o


 def __str__(self):
  if self.__tt == LUA_TSTRING:
   return '"%s"' % self.__o
  elif self.__tt == LUA_TBOOLEAN:
   return 'true' if self.__o != 0 else 'false'
  elif self.__tt == LUA_TNUMBER:
   return '%.7g' % self.__o
  elif self.__tt == LUA_TNIL:
   return 'nil'
  else:
   return 'UNKNOWN[%u, %s]' % (self.__tt, str(self.__o))


 def __repr__(self):
  return '(TValue %s | %s)' % (LUA_TYPE_NAMES[self.__o] if self.__o <= LUA_TDEADKEY else str(self.__o), self.__str__())


 def GetType(self):
  return self.__tt


 def GetValue(self):
  return self.__o


 def IsString(self):
  return self.__tt == LUA_TSTRING


 def IsNull(self):
  return self.__tt == LUA_TNIL


class LocVar(object):
 """LocVar"""

 """Class constructor"""
 def __init__(self, name, start, end):
  self.__name = name
  self.__start_pc = start
  self.__end_pc = end


 def GetName(self):
  return self.__name


 def GetStart(self):
  return self.__start_pc


 def GetEnd(self):
  return self.__end_pc


class Stack(object):
 def __init__(self):
  self.__items = []

 def empty(self):
  return self.__items == []

 def push(self, item):
  self.__items.append(item)

 def pop(self):
  return self.__items.pop()

 def peek(self):
  return self.__items[len(self.__items)-1]

 def size(self):
  return len(self.__items)
