#!/usr/bin/python
# -*- coding: utf-8 -*-

OP_MOVE = 0
OP_LOADK = 1
OP_LOADBOOL = 2
OP_LOADNIL = 3
OP_GETUPVAL = 4

OP_GETGLOBAL = 5
OP_GETTABLE = 6

OP_SETGLOBAL = 7
OP_SETUPVAL = 8
OP_SETTABLE = 9

OP_NEWTABLE = 10

OP_SELF = 11

OP_ADD = 12
OP_SUB = 13
OP_MUL = 14
OP_DIV = 15
OP_MOD = 16
OP_POW = 17
OP_UNM = 18
OP_NOT = 19
OP_LEN = 20

OP_CONCAT = 21

OP_JMP = 22

OP_EQ = 23
OP_LT = 24
OP_LE = 25

OP_TEST = 26
OP_TESTSET = 27 

OP_CALL = 28
OP_TAILCALL = 29
OP_RETURN = 30

OP_FORLOOP = 31
OP_FORPREP = 32
OP_TFORLOOP = 33

OP_SETLIST = 34

OP_CLOSE = 35
OP_CLOSURE = 36

OP_VARARG = 37


MAXARG_OPCODE = 0x3F
MAXARG_A = 0xFF
MAXARG_B = 0x1FF
MAXARG_C = 0x1FF
MAXARG_Bx = 0x3FFFF
MAXARG_sBx = MAXARG_Bx >> 1

BITRK = 0x100

def GET_OPCODE(i):
 return i & MAXARG_OPCODE

def GETARG_A(i):
 return (i >> 6) & MAXARG_A

def GETARG_B(i):
 return (i >> 23) & MAXARG_B

def GETARG_C(i):
 return (i >> 14) & MAXARG_C

def GETARG_Bx(i):
 return (i >> 14) & MAXARG_Bx

def GETARG_sBx(i):
 return GETARG_Bx(i)-MAXARG_sBx

def ISK(x):
 return x & BITRK

def INDEXK(r):
 return r & ~BITRK

def ISCONDJMP(x):
 return x == OP_TEST or x == OP_EQ or x == OP_LT or x == OP_LE