#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
from luaconf import *

class RegisterError(Exception):
 """Reigster exception class"""

 def __init__(self, msg):
  self.message = msg


class RegisterStack(object):
 """Stack class"""

 """Class constructor"""
 def __init__(self, name, prefix, size):
  self.__entries = [None] * size
  self.__stack_size = size
  self.__prefix = prefix
  self.__name = name


 def SetEntry(self, id, reg):
  if id >= self.__stack_size:
   raise RegisterError('Accessed entry %s%u out of bounds. %s stack size is %u' % (self.__prefix, id, self.__name, self.__stack_size))

  self.__entries[id] = reg


 def GetEntry(self, id):
  if id >= self.__stack_size:
   raise RegisterError('Accessed entry %s%u out of bounds. %s stack size is %u' % (self.__prefix, id, self.__name, self.__stack_size))

  return self.__entries[id]


class RegisterBase(object):
 """Register base class"""

 """Class constructor"""
 def __init__(self):
  pass


 def Cast(self, t):
  _self = self.GetSelf()
  return _self if isinstance(_self, t) else None


 def Eval(self, f):
  return self.__str__()


 def GetSelf(self):
  return self


 def IsA(self, t):
  return isinstance(self, t)


 def IsNull(self):
  return False


 def GetPriority(self):
  return PRIORITY_ATOMIC


 def GetAssociativity(self):
  return ASSOCIATIVITY_NONE


class BinaryConditionalRegister(RegisterBase):
 """Binary conditional register class"""

 """Class constructor"""
 def __init__(self, a, b, c, op, invop):
  RegisterBase.__init__(self)
  self.__invop = invop
  self.__op = op
  self.__a = a
  self.__b = b
  self.__c = c


 def Invert(self):
  return BinaryConditionalRegister(self.__a, self.__b,
                not self.__c, self.__op, self.__invop)


 def Eval(self, f):
  if not isinstance(self.__a, RegisterBase):
   print('ALHARMA!', str(self.__a))

  return '%s %s %s' % (
  self.__a.Eval(f), 
  self.__op if self.__c else self.__invop,
  self.__b.Eval(f))


class TestRegister(RegisterBase):
 """Binary conditional register class"""

 """Class constructor"""
 def __init__(self, reg, flag):
  RegisterBase.__init__(self)
  self.__flag = flag
  self.__reg = reg


 def Invert(self):
  return TestRegister(self.__reg, not self.__flag)


 def Eval(self, f):
  return '%s%s'% ('' if self.__flag != 0 else 'not ', self.__reg.Eval(f))


class CallRegister(RegisterBase):
 """Call register class"""

 """Class constructor"""
 def __init__(self, c, p):
  RegisterBase.__init__(self)
  self.__params = p
  self.__call = c


 def Eval(self, f):
  if self.__params == None:
   params = ''
  elif isinstance(self.__params, list):
   _self = self.__call.IsA(SelfEntryRegister)
   params = ', '.join([p.Eval(f) for p in (self.__params[1:] if _self else self.__params)])
  else:
   _self = self.__call.IsA(SelfEntryRegister)
   params = '' if _self else self.__params.Eval(f)

  return '%s(%s)' % (self.__call.Eval(f), params)


class MultiRegister(RegisterBase):
 def __init__(self, reg, size):
  RegisterBase.__init__(self)
  self.__size = size
  self.__reg = reg

 def GetSize(self):
  return self.__size

 def Eval(self, f):
  return self.__reg.Eval(f)

class UpvalueRegister(RegisterBase):
 """UpValue register class"""

 """Class constructor"""
 def __init__(self, upvalue):
  RegisterBase.__init__(self)
  self.__upvalue = upvalue


 def GetSelf(self):
  return self.__upvalue.GetSelf()


 def Eval(self, f):
  return self.GetSelf().Eval(f)


class ReferencedRegister(RegisterBase):
 """Referenced register class"""

 """Class constructor"""
 def __init__(self, ref):
  RegisterBase.__init__(self)
  self.__ref = ref


 def GetSelf(self):
  _self = self
  _rself = self.__ref

  while True:
   if _self == _rself:
    return _self

   _self = _rself
   _rself = _self.GetSelf()


 def GetPriority():
  return self.GetSelf().GetPriority()


 def IsNull():
  return self.GetSelf().IsNull()


 def IsA(self, t):
  return self.GetSelf().IsA(t)


 def Eval(self, f):
  return self.GetSelf().Eval(f)


class ConstantRegister(RegisterBase):
 """Constant register class"""

 """Class constructor"""
 def __init__(self, val):
  RegisterBase.__init__(self)
  self.__value = val


 def GetTValue(self):
  return self.__value


 def IsNull(self):
  return self.__value.IsNull()


 def __str__(self):
  return str(self.__value)


 def Eval(self, f):
  return str(self.__value)


class GlobalRegister(RegisterBase):
 """Global register class"""

 """Class constructor"""
 def __init__(self, val):
  RegisterBase.__init__(self)
  self.__global = val


 def Eval(self, f):
  return self.__global


class VarargRegister(RegisterBase):
 """VarArg register class"""

 """Class constructor"""
 def __init__(self, flags):
  RegisterBase.__init__(self)
  self.__flags = flags

 def Eval(self, f):
  return '...' if (self.__flags & VARARG_NEEDSARG) == 0 else 'arg'


class ArgumentRegister(RegisterBase):
 """Argument register class"""

 """Class constructor"""
 def __init__(self, name):
  RegisterBase.__init__(self)
  self.__name = name

 def Eval(self, f):
  return self.__name


class LocVarRegister(RegisterBase):
 """Argument register class"""

 """Class constructor"""
 def __init__(self, name, r = None):
  RegisterBase.__init__(self)
  self.__name = name
  self.__reg = r


 def Eval(self, f):
  if self.__reg != None:
   ret = self.__reg.Eval(f)
   ret = re.sub(r'require\(\"(.+?)\"\)', r'\1', ret)
   return ret
  else:
   return self.__name 


class TableRegister(RegisterBase):
 """Table register class"""

 """Class constructor"""
 def __init__(self, size):
  RegisterBase.__init__(self)
  self.__list = [None] * size
  self.__table = {}


 def __get_key(self, f, a):
  a = a.GetSelf()
  if not a.IsA(ConstantRegister):
   raise RegisterError('Table entry key accessed without being a ConstantRegister')

  val = a.GetTValue()
  return val.GetValue() if val.IsString() else '[%s]' % val


 def __get_val(self, f, a):
  return a.Eval(f)


 def Set(self, key, value):
  self.__table[key] = value


 def Insert(self, i, val):
  if i > len(self.__list):
   raise RegisterError('Inserting value out of bounds (%u/%u).' % (i, self.__list_size))

  self.__list.insert(i, val)


 def Eval(self, f):
  parts = []
  if self.__table:
   parts.append('{%s}' % ', '.join(['%s = %s' % (
   self.__get_key(f, k), self.__get_val(f, v)) 
   for k, v in self.__table.items()]))

  if self.__list:
   i = len(self.__list)-1
   while i >= 0 and self.__list[i] == None:
    i-=1

   parts.append('{%s}' % ', '.join([self.__get_val(f, k) for k in self.__list[:i+1]]))

  return '{%s}' % '; '.join(parts)



class TableEntryRegister(RegisterBase):
 """Table register class"""

 """Class constructor"""
 def __init__(self, table, key):
  RegisterBase.__init__(self)
  self.__table = table
  self.__key = key


 def Eval(self, f):
  table = self.__table.Eval(f)
  key = self.__key.Eval(f) if isinstance(self.__key, RegisterBase) else str(self.__key)
  if key != None and key.find('"') != -1:
   return '%s.%s' % (table, key.replace('"', ''))
  else:
   return '%s[%s]' % (table, key)


class SelfEntryRegister(RegisterBase):
 """Table register class"""

 """Class constructor"""
 def __init__(self, selfreg, key):
  RegisterBase.__init__(self)
  self.__self = selfreg
  self.__key = key


 def Eval(self, f):
  return '%s:%s' % (self.__self.Eval(f), (self.__key.Eval(f) 
  if isinstance(self.__key, RegisterBase) else self.__key.GetValue()).replace('"', ''))


class BinaryOperatorRegister(RegisterBase):
 """Binary operator register class"""

 """Class constructor"""
 def __init__(self, a, b):
  RegisterBase.__init__(self)
  self.__a = a
  self.__b = b


 def __get_operand(self, f, o):
  priority = self.GetPriority()
  o = o.GetSelf() if isinstance(o, RegisterBase) else o

  if isinstance(o, BinaryOperatorRegister) and o.GetPriority() <= priority:
   return '(%s)' % o.Eval(f)
  elif isinstance(o, RegisterBase):
   return o.Eval(f)
  else:
   return str(o)


 def Eval(self, f, op):
  return '%s %s %s' % (self.__get_operand(f, self.__a), op, self.__get_operand(f, self.__b))


class ConcatRegister(RegisterBase):
 """Concat register class"""

 """Class constructor"""
 def __init__(self, params):
  RegisterBase.__init__(self)
  self.__params = params

 def Eval(self, f):
  return ' .. '.join([p.Eval(f) for p in self.__params])


class AddRegister(BinaryOperatorRegister):
 """Add register class"""

 """Class constructor"""
 def __init__(self, a, b):
  BinaryOperatorRegister.__init__(self, a, b)


 def Eval(self, f):
  return BinaryOperatorRegister.Eval(self, f, '+')


 def GetPriority(self):
  return 1


class SubRegister(BinaryOperatorRegister):
 """Sub register class"""

 """Class constructor"""
 def __init__(self, a, b):
  BinaryOperatorRegister.__init__(self, a, b)


 def Eval(self, f):
  return BinaryOperatorRegister.Eval(self, f, '-')


 def GetPriority(self):
  return 1


class MulRegister(BinaryOperatorRegister):
 """Mul register class"""

 """Class constructor"""
 def __init__(self, a, b):
  BinaryOperatorRegister.__init__(self, a, b)


 def Eval(self, f):
  return BinaryOperatorRegister.Eval(self, f, '*')


 def GetPriority(self):
  return 2


class DivRegister(BinaryOperatorRegister):
 """Div register class"""

 """Class constructor"""
 def __init__(self, a, b):
  BinaryOperatorRegister.__init__(self, a, b)


 def Eval(self, f):
  return BinaryOperatorRegister.Eval(self, f, '/')


 def GetPriority(self):
  return 2


class ModRegister(BinaryOperatorRegister):
 """Mod register class"""

 """Class constructor"""
 def __init__(self, a, b):
  BinaryOperatorRegister.__init__(self, a, b)


 def Eval(self, f):
  return BinaryOperatorRegister.Eval(self, f, '%')


 def GetPriority(self):
  return 2


class PowRegister(BinaryOperatorRegister):
 """Pow register class"""

 """Class constructor"""
 def __init__(self, a, b):
  BinaryOperatorRegister.__init__(self, a, b)


 def Eval(self, f):
  return BinaryOperatorRegister.Eval(self, f, '^')


 def GetPriority(self):
  return 3


class UnaryOperatorRegister(RegisterBase):
 """Unary operator register class"""

 """Class constructor"""
 def __init__(self, a):
  RegisterBase.__init__(self)
  self.__a = a


 def GetOperand(self):
  return self.__a


class NegRegister(UnaryOperatorRegister):
 """Neg register class"""

 """Class constructor"""
 def __init__(self, a):
  UnaryOperatorRegister.__init__(self, a)


 def Eval(self, f):
  return '-%s' % self.GetOperand().Eval(f)


class NotRegister(UnaryOperatorRegister):
 """Not register class"""

 """Class constructor"""
 def __init__(self, a):
  UnaryOperatorRegister.__init__(self, a)


 def Eval(self, f):
  return 'not %s' % self.GetOperand().Eval(f)


class LengthRegister(UnaryOperatorRegister):
 """Length register class"""

 """Class constructor"""
 def __init__(self, a):
  UnaryOperatorRegister.__init__(self, a)
  self.__a = a


 def Eval(self, f):
  return '#%s' % self.__a.Eval(f)


class ClosureRegister(RegisterBase):
 """Closure register class"""

 """Class constructor"""
 def __init__(self, f):
  RegisterBase.__init__(self)
  self.__closure = f


 def Eval(self, f):
  p = self.__closure.GetPrototype()
  return 'function (%s) -- [VARARG: %u] <%s>' % (', '.join(self.__closure.GetParamNames()), p.IsVariableArg(), ', '.join([self.__closure.GetUpValue(i).Eval(self.__closure) for i in range(0, p.GetUpValueCount())]))
