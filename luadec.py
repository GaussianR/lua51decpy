#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import utils
import lundump
import lregister
import ldecompile
import linstruction


"""Main function"""
def main(argc, argv):
 if argc < 2:
  print('[ERROR] Syntax error. Right syntax is %s <file.lua>' % utils.GetPathFileName(argv[0]))
  return -1

 luapath = argv[1]

 if __debug__:
  print('')
  print('[INFO] Decompiling file "%s"' % luapath)

 try:
  sc = ldecompile.Decompile(luapath)
 except ldecompile.DecompileError as e:
  print('[EXCEPTION] %s' % e.message)
  return -2

 if not __debug__:
  print(sc.GenerateSource())

 return 0

if __name__ == '__main__':
 main(len(sys.argv), sys.argv)
