#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import codecs
import struct

import lfunc
import lobject
from luaconf import *

LUAC_FORMAT = 0
LUAC_VERSION = 0x51
LUAC_HEADERMAGIC = 0x61754C1B

class UndumpError(Exception):
 """Undump exception"""

 """Class constructor"""
 def __init__(self, msg):
  self.message = msg

class Undump(object):
 """LUA Undumper class"""

 """Class constructor"""
 def __init__(self, filename):
  self.__file_handle = open(filename, 'rb')
  if not self.__file_handle:
   raise IOError

  self.__read_header()
  self.__prototype = self.__load_function('0')


 def GetMainProto(self):
  return self.__prototype

 """Helper function to load a fixed structure from file"""
 def __dump_struct(self, fmt, vars):
  result = {}

  data = self.__file_handle.read(struct.calcsize(fmt))
  if data == None:
   return None

  p = struct.unpack(fmt, data)
  for i in range(len(vars)):
   result[vars[i]] = p[i]

  return result


 """Read lua file header"""
 def __read_header(self):
  header = self.__dump_struct('I' + 'B' * 8,
  ['magic', 'version', 'format', 'endianness',
   'int_type', 'size_type', 'instruction_type',
   'number_type', 'number_integral'])

  if header == None:
   raise UndumpError('Couldn\' read header for lua file.')

  if header['magic'] != LUAC_HEADERMAGIC:
   raise UndumpError('Lua file header has invalid signature.')

  if header['version'] != LUAC_VERSION:
   raise UndumpError('Lua file header version isn\'t 5.1')

  if header['format'] != LUAC_FORMAT:
   raise UndumpError('Lua file header has invalid format.')

  self.__endianness = '<' if header['endianness'] == 1 else '>'

  type = header['int_type']
  self.__int_type = 'i' if type == 4 else (
                    'q' if type == 8 else (
                    'h' if type == 2 else 'c'))

  type = header['size_type']
  self.__size_type = 'i' if type == 4 else (
                     'q' if type == 8 else (
                     'h' if type == 2 else 'c'))

  type = header['instruction_type']
  self.__instruction_type = 'I' if type == 4 else (
                            'Q' if type == 8 else (
                            'H' if type == 2 else 'C'))

  type = header['number_type']
  self.__number_type = 'f' if type == 4 else 'd'

  self.__number_integral = header['number_integral']


 """Load an int8"""
 def __load_char(self):
  data = self.__file_handle.read(1)
  if not data:
   return None

  data = struct.unpack('b', data)
  return data[0] if data else None


 """Load a uint8"""
 def __load_byte(self):
  data = self.__file_handle.read(1)
  if not data:
   return None

  data = struct.unpack('B', data)
  return data[0] if data else None


 """Load an integer"""
 def __load_int(self):
  fmt = self.__endianness + self.__int_type
  data = self.__file_handle.read(struct.calcsize(fmt))
  if data == None:
   return None

  data = struct.unpack(fmt, data)
  return data[0] if data else None


 """Load size type"""
 def __load_size(self):
  fmt = self.__endianness + self.__size_type
  data = self.__file_handle.read(struct.calcsize(fmt))
  if data == None:
   return None

  data = struct.unpack(fmt, data)
  return data[0] if data else None


 """Load a number"""
 def __load_number(self):
  fmt = self.__endianness + self.__number_type
  data = self.__file_handle.read(struct.calcsize(fmt))
  if data == None:
   return None

  data = struct.unpack(fmt, data)
  return data[0] if data else None


 """Load a string"""
 def __load_string(self):
  size = self.__load_size()
  if size == None:
   return None
  elif size == 0:
   return ''

  _str = self.__file_handle.read(size)
  return (b'' if _str[0] == 0 else _str[:-1]).decode("utf-8")


 """Load a count type vector"""
 def __load_vector(self, type, count):
  fmt = type * count

  data = self.__file_handle.read(struct.calcsize(fmt))
  if not data:
   return None

  return struct.unpack(fmt, data)


 """Load instructions"""
 def __load_code(self):
  size = self.__load_int()
  if size == None:
   return None

  return self.__load_vector(self.__instruction_type, size)


 """Load constants"""
 def __load_constants(self, f):
  name = f.GetName()

  n = self.__load_int()
  if n == None:
   raise UndumpError('Couldn\'t obtain constants count for "%s" closure.' % name)

  k = [None] * n
  for i in range(0, n):
   t = self.__load_char()
   if t == None:
    raise UndumpError('Couldn\'t obtain K%u type for "%s" closure.' % (i, name))

   o = None
   if t == LUA_TBOOLEAN:
    o = self.__load_char()
    if o == None:
     raise UndumpError('Couldn\'t obtain K%u[%s] value for "%s" closure.' % (i, LUA_TYPE_NAMES[t], name))
    else:
     o = o != 0
   elif t == LUA_TNUMBER:
    o = self.__load_number()
    if o == None:
     raise UndumpError('Couldn\'t obtain K%u[%s] value for "%s" closure.' % (i, LUA_TYPE_NAMES[t], name))
   elif t == LUA_TSTRING:
    o = self.__load_string().encode('unicode-escape').decode('utf-8')
    if o == None:
     raise UndumpError('Couldn\'t obtain K%u[%s] value for "%s" closure.' % (i, LUA_TYPE_NAMES[t], name))
   elif t != LUA_TNIL:
    raise UndumpError('Unknown K%u type for "%s" closure.' % (i, name))

   k[i] = lobject.TValue(t, o)

   if __debug__:
    print ('[%s] - K%u[%s] := "%s"' % (name, i, LUA_TYPE_NAMES[t], str(o)))

  f.SetConstants(k)

  n = self.__load_int()
  if n == None:
   raise UndumpError('Couldn\'t obtain closures count for "%s" closure.' % name)

  p = [None] * n
  for i in range(0, n):
   p[i] = self.__load_function('%s_%u' % (name, i))

  f.SetPrototypes(p)


 """Load Debug information"""
 def __load_debug(self, f):
  name = f.GetName()

  n = self.__load_int()
  if n == None:
   raise UndumpError('Couldn\'t obtain lineinfo count for "%s" closure.' % name)

  linfo = self.__load_vector(self.__int_type, n)

  lineinfo = {}
  for i in range(0, len(linfo)):
   line = linfo[i]
   if line not in lineinfo:
    lineinfo[line] = [i, i]
   else:
    lineinfo[line][1] = max(lineinfo[line][1], i)

  if lineinfo == None:
   raise UndumpError('Couldn\'t obtain lineinfo for "%s" closure.' % name)
  else:
   f.SetLineInfo(lineinfo)

  n = self.__load_int()
  if n == None:
   raise UndumpError('Couldn\'t obtain lineinfo count for "%s" closure.' % name)

  if __debug__:
   print ('Closure "%s" has %u local variables.' % (name, n))
   print ('')

  lvars = [None] * n
  for i in range(0, n):
   lname = self.__load_string()
   if lname == None:
    raise UndumpError('Couldn\'t obtain %u lvar name for "%s" closure.' % (i, name))

   lstart = self.__load_int()
   if lstart == None:
    raise UndumpError('Couldn\'t obtain %u lvar start PC for "%s" closure.' % (i, name))

   lend = self.__load_int()
   if lend == None:
    raise UndumpError('Couldn\'t obtain %u lvar end PC for "%s" closure.' % (i, name))

   lvars[i] = lobject.LocVar(lname, lstart, lend)

   if __debug__:
    print ('[%s] - LVAR%u|%08X-%08X| := "%s"' % (name, i, lstart, lend, lname))

  f.SetLocalVars(lvars)

  n = self.__load_int()
  if n == None:
   raise UndumpError('Couldn\'t obtain lineinfo count for "%s" closure.' % name)

  upvalues = [None] * n
  for i in range(0, n):
   upname = self.__load_string()
   if upname == None:
    raise UndumpError('Couldn\'t obtain U%u name for "%s" closure.' % (i, name))

   upvalues[i] = upname

  f.SetUpValueNames(upvalues)


 """Load a closure"""
 def __load_function(self, name):
  source = self.__load_string()
  name = source if source else name

  line = self.__load_int()
  if line == None:
   raise UndumpError('Couldn\'t obtain first line number for "%s" closure.' % name)

  lastline = self.__load_int()
  if lastline == None:
   raise UndumpError('Couldn\'t obtain last line number for "%s" closure.' % name)

  nups = self.__load_byte()
  if nups == None:
   raise UndumpError('Couldn\'t obtain upvalue number for "%s" closure.' % name)

  nparams = self.__load_byte()
  if nparams == None:
   raise UndumpError('Couldn\'t obtain fixed args count for "%s" closure.' % name)

  isvararg = self.__load_byte()
  if isvararg == None:
   raise UndumpError('Impossible to determine wether "%s" closure has or not variable arguments.' % name)

  maxstacksize = self.__load_byte()
  if maxstacksize == None:
   raise UndumpError('Couldn\'t max stack size for "%s" closure.' % name)

  p = lfunc.Proto(
   name, line, lastline, nups,
   nparams, isvararg, maxstacksize
  )

  code = self.__load_code()
  if code == None:
   raise UndumpError('Couldn\'t read code for "%s" closure.' % name)
  else:
   p.SetCode(code)

  if __debug__:
   print ('')
   print ('Closure "%s" has %u instructions, %u args, %u upvalues.' % (name, len(code), nparams, nups))

  self.__load_constants(p)
  self.__load_debug(p)
  return p