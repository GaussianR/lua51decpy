#!/usr/bin/python
# -*- coding: utf-8 -*-

def GetPathFileName(path):
 r = path.rfind('/')
 if r == -1:
  r = path.rfind('\\')

 return path[r+1:]


def GetTruthTable(n):
 return [[((i >> j) & 1) for j in range(0, n)] for i in range(0, (1 << n))]
